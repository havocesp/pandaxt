# -*- coding:utf-8 -*-
"""
A Python 3 "ccxt" wrapper over "Pandas" lib.
"""
__name__ = 'PandaXT'
__package__ = 'pandaxt'
__author__ = 'Daniel J. Umpierrez'
__license__ = 'MIT'
__version__ = '0.1.4'
__description__ = 'A Python 3 "ccxt" wrapper over "Pandas" lib.'
__site__ = 'https://github.com/havocesp/{}'.format(__package__)
__email__ = 'umpierrez@pm.me'
__keywords__ = ['altcoins', 'altcoin', 'exchange']

from pandaxt.core import PandaXT

__all__ = ['__description__', '__author__', '__license__', '__version__', '__package__', '__name__', '__site__',
           '__email__', '__keywords__', 'PandaXT']
